import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import{
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  HttpLink,
  from,
  useQuery,
  gql
} from "@apollo/client";
import { onError } from '@apollo/client/link/error';
import { graphql } from 'graphql';

const errorLink = onError(
  ({graphQLErrors, networkError})=>{
  //alert("errorLink types " + typeof(graphQLErrors)/* + " " + typeof(networkErrors)*/)
  if(graphQLErrors){
    graphQLErrors.map(({message, locations, path})=>{
      alert('GraphQL error ${message} [${location}] [${path}]');
    })
  }
  if(networkError){    alert(networkError['message'])  }
})

const link = from([
  errorLink,
  new HttpLink({uri:"http://localhost:4000/graphql"})
])

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: link
})


const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <ApolloProvider client={client}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </ApolloProvider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
