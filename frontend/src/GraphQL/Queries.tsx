import {gql} from '@apollo/client'

export const TEST1 = gql`
  query ExampleQuery{
    getProposition(id: "test1") {
      id
      name
    }
  }
`