import React, { useState, useEffect } from 'react';

import logo from './logo.svg';
import './App.css';
import{useQuery} from "@apollo/client";
import {TEST1} from './GraphQL/Queries'

/*
async function getProposition(propId:string) {
  const response = await fetch('http://localhost:4000/graphql', {
    method:'POST',
    headers:{'content-type':'application/json'},
    body:JSON.stringify({
      'query': 'query ExampleQuery{\n  getProposition(id: "'+propId+'") {\n    id\n    name\n  }\n}',
      'variables': {},
    })
  })
  const rsponseBody = await response.json();
  return rsponseBody;
}*/


function App() {

  const {error, loading, data} = useQuery(TEST1);
  var [rsp,chrsp]=useState("fetching")
  useEffect(()=>{
    if(data){
      chrsp(data['getProposition']['name'])
    }
  }, [data])

  /*let prop = getProposition('test1')
  console.log("LOG")
  prop.then(result =>{chrsp(result['data']['getProposition']['name']);})
  console.log(prop)*/
  
  return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.tsx</code> and save to reload.
          </p>
          <p id="promised">
            Response to test1 is {rsp}
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
  );
}

export default App;
