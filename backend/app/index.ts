import { ApolloServer } from "apollo-server-express";
import { Schema } from "./schema";
import express from "express";
import { ApolloServerPluginDrainHttpServer } from "apollo-server-core";
import http from "http";
import testDatabase from "./sequelizetest";
import * as unzip from 'unzip-stream';
import { Transform } from 'stream';
import * as fs from 'fs';
import { Users,
  Comments,
  Politician,
  PoliticalParty,
  Rating,
  Proposition,
  PropositionCreators,
  PropositionParty } from "./db";
import sequelize from "./databaseConnection";
import { Resolvers } from "./resolvers";
import retrieve_proposition  from './extractdatajson'



async function startApolloServer(schema: any, resolvers: any) {
  const app = express();
  const httpServer = http.createServer(app);
  
  await initDatabase()
  await testDatabase()

  const server = new ApolloServer({
    typeDefs: schema,
    resolvers,
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
  }) as any;
  await server.start();
  server.applyMiddleware({ app });
  await new Promise<void>((resolve) =>
    httpServer.listen({ port: 4000 }, resolve)
  );
  console.log(`Server ready at http://localhost:4000${server.graphqlPath}`);
  
  /*
  fs.createReadStream('/usr/src/app/data/sample_data.zip')
    .pipe(unzip.Extract({ path: '/usr/src/app/data/' }));
  /*/
 
  
  //fs.createReadStream('path/to/archive.zip')
  fs.createReadStream('/usr/src/app/data/sample_data.zip')
    .pipe(unzip.Extract({ path: '/usr/src/app/data' }))
    .on('close',retrieve_proposition);
  
  /*
  fs.createReadStream('/usr/src/app/data/sample_data.zip')
  .pipe(unzip.Parse())
  .on('entry', (entry: unzip.Entry) => {
      const fileName = entry.path;
      const type = entry.type; // 'Directory' or 'File'
      const size = entry.size;
      if (fileName === "this IS the file I'm looking for") {
          entry.pipe(fs.createWriteStream('/usr/path/'));
      } else {
          entry.autodrain();
      }
  });//*/
}

startApolloServer(Schema, Resolvers);

async function initDatabase() {
  try {
    await sequelize.authenticate();
console.log('Connection to database has been established successfully.');
} catch (error) {
    console.error('Unable to connect to the database:', error);
}


await Users.sync({alter: true});
  await Comments.sync({alter: true});
  await Politician.sync({alter: true});
  await Proposition.sync({alter: true});
  await PoliticalParty.sync({alter: true});
  await Rating.sync({alter: true});
  await sequelize.sync({alter: true})


Users.hasMany(Rating);
Users.hasMany(Comments);

Comments.belongsTo(Users, {
    foreignKey: {
      allowNull: false
    }
  });
  Comments.belongsTo(Proposition, {
    foreignKey: {
      allowNull: true
    }
  });

  PoliticalParty.hasMany(Politician);
  PoliticalParty.belongsToMany(Proposition, {through: PropositionParty});

  Politician.belongsToMany(Proposition,{through: PropositionCreators});
  Politician.belongsTo(PoliticalParty);

  Proposition.belongsToMany(PoliticalParty, {through: PropositionParty})
  Proposition.belongsToMany(Politician,{through: PropositionCreators});
  Proposition.hasMany(Comments);
  Proposition.hasMany(Rating)

  Rating.belongsTo(Users, {
    foreignKey: {
      allowNull: false
    }
  });
  Rating.belongsTo(Proposition, {
    foreignKey: {
      allowNull: false
    }
  });

  await sequelize.sync({alter: true})
  await Users.sync({alter: true});
  await Comments.sync({alter: true});
  await Politician.sync({alter: true});
  await Proposition.sync({alter: true});
  await PoliticalParty.sync({alter: true});
  await Rating.sync({alter: true});
  await PropositionCreators.sync({alter: true});
  await PropositionParty.sync({alter: true});
  
}