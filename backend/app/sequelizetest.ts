import {Sequelize, DataTypes} from 'sequelize';
import {
  Comments,
  Users} from './db'

async function testDatabase(){
    console.log("Database inited");
      var u1 = await Users.create({
        pseudo:'usertest',
        password:'mysupersecretpass',
        email:'usertest@gmail.com' 
    });
    
    
    const c1=await Comments.create({  
        Users: u1,
        text:'Vraiment un super article'
    });

    const c2=await Comments.create({
        Users: u1,
        text:'Pire article que je n\'ai jamais lue'
    });


    c2.destroy();
    c1.destroy();
    u1.destroy(); 
    console.log("Test checked");
}

export default testDatabase;