import Proposition from "./dataset";
import { Comments } from "./db/CommentEntity";
import { PoliticalParty } from "./db/PoliticalParty";
import { Politician } from "./db/Politician";
import { Rating } from "./db/Rating";

export const Resolvers = {
  Query: {
    getAllComments: () => Comments.findAll(), 
    getComment: (_: any, args: any) => { 
      console.log(args);
      return Comments.findByPk(args.id);
    },
    getAllParties: () => PoliticalParty.findAll(), 
    getParties: (_: any, args: any) => { 
      console.log(args);
      return PoliticalParty.findByPk(args.id);
    },
    getAllPoliticians: () => Politician.findAll(), 
    getPolitician: (_: any, args: any) => { 
      console.log(args);
      return Politician.findByPk(args.id);
    },
    getAllPropositions: () => Proposition.findAll(), 
    getProposition: (_: any, args: any) => { 
      console.log(args);
      return Proposition.findByPk(args.id);
    },
    getAllRatings: () => Rating.findAll(), 
    getRating: (_: any, args: any) => { 
      console.log(args);
      return Rating.findByPk(args.id);
    },
  },
};