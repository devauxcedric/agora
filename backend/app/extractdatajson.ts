import * as fs from 'fs';


//import * from 'unzip-stream';
//var unzip = require('unzip-stream')

/*
function recursive_extraction(json_: any, filename: string, extraction_array: Array<string>, previous_prefix: string){
  //Object.keys(extraction_array[0]).forEach((current_prefix: string)=>{
  const current_prefix=extraction_array.shift();
    if(current_prefix){
      //console.log("["+current_prefix)
      if(json_ == undefined) return ;
      if(Object.keys(json_) == undefined) return ;
      if(Object.keys(json_).includes(current_prefix) && json_[current_prefix] != null){
        //console.log("Tra tra " + Object.keys(json_));
        recursive_extraction(json_[current_prefix],
                            filename,
                            extraction_array,
                            previous_prefix + current_prefix+".")
      }else{
        
        console.log("NULL")
        console.log(previous_prefix+current_prefix+" " + filename)
        console.log(Object.keys(json_) + " " + typeof(json_))
        return json_;

      }
    }else{
      return json_;
      console.log("ggg " + previous_prefix+" " + filename)
      console.log(Object.keys(json_))
      //console.log(json_)
    }
  //})  
}//*/

/*
function recursive_aaa(json_: any, filename: string, extraction_array: any, previous_prefix: string){
  if(json_ == undefined) return;
  if(Object.keys(json_)==undefined)return;
  Object.keys(json_).forEach((current_prefix: string)=>{
    if(current_prefix == "callback"){
      return 
    }
    console.log("["+current_prefix)
    recursive_aaa(json_[current_prefix], filename, extraction_array[current_prefix], current_prefix)
  })
}*/

function find_keywords_in_json(keywords: Array<string>, dictionnary: any, path: Array<string>): Array<string>{
    if(dictionnary == null ||
       dictionnary == undefined ||
       Object.keys(dictionnary) == undefined|| 
       Object.keys(dictionnary) == null){
        return [""]
       }
    //console.log(path)
    let dic: Array<string>=[]
    Object.keys(dictionnary).forEach((current_prefix: string)=>{
      //console.log("KEY " + current_prefix + " KEYS " + Object.keys(dictionnary))
      keywords.forEach((key:string)=>{
        if(current_prefix == key){
          dic.push(dictionnary[key])
          //console.log(dic);
          //return dic;
        }
      })
  
      if(Object.keys(dictionnary) != null){
        let ndic: Array<string>=[];
        if(isNaN(parseInt(current_prefix))){
          let npath = path;
          npath.push(current_prefix);
          ndic = find_keywords_in_json(keywords, dictionnary[current_prefix], npath);
        }
        if(ndic.length != 0){
          dic.concat(ndic)
        }
      }
      
    })
    return dic;
  }

  //*
  //document.dossierParlementaire.actesLegislatifs
  function actesLegislatifs2_(json_: any, filename: string, previous_prefix:string){
    if(json_ == null || Object.keys(json_)==undefined){return;}
    const current_prefix="actesLegislatifs"
    process.stdout.write(current_prefix+"->")
    //document.dossierParlementaire.acteLegislatif.actesLegislatifs
    if(Object.keys(json_).includes(current_prefix)){
      //acteLegislatif_(json_[current_prefix], filename, current_prefix+".")
      console.log(json_[""])
    }else{
      console.log("actesLegislatifs2_")
      console.log(previous_prefix+current_prefix+" " + filename)
      console.log(Object.keys(json_))
      console.log(json_)
      process.exit()
    }
  }
  
  const grnTxtCns="\x1b[32;1m";
  const redTxtCns="\x1b[31;1m";
  const clrTxtCns="\x1b[0m";

  function acteLegislatif_(json_: any, filename: string, previous_prefix:string){
    if(json_ == null || Object.keys(json_)==undefined){return;}
    const current_prefix="acteLegislatif"
    process.stdout.write(current_prefix+"->")
    //document.dossierParlementaire.actesLegislatifs.acteLegislatif
    if(Object.keys(json_).includes(current_prefix)){
      //acteLegislatif_(json_.dossierParlementaire, filename, "acteLegislatif.")
      //console.log(json_[current_prefix])
      if( Object.keys(json_[current_prefix]).includes("texteAssocie") ){
        //console.log(json_)
        console.log(filename + " " + json_[current_prefix])
        process.stdout.write(grnTxtCns+"("+
            json_[current_prefix]["libelleActe"]['nomCanonique']+
            ","+
            json_[current_prefix]["texteAssocie"]+")"+clrTxtCns)
        //actesLegislatifs_(json_[current_prefix], filename, previous_prefix+current_prefix+".   ")
      }else if (Object.keys(json_[current_prefix]).includes("actesLegislatifs")){
        actesLegislatifs_(json_[current_prefix], filename,  previous_prefix+current_prefix+".")
      }else{
        Object.keys(json_[current_prefix]).forEach((key: string)=>{
          //console.log("KEY " + key)
          //process.stdout.write("["+key+']'+'('+Object.keys()+')')
          if(Object.keys(json_[current_prefix][key]).includes("texteAssocie")){
            //console.log(json_[current_prefix])
            process.stdout.write(redTxtCns+"("+
            json_[current_prefix][key]["libelleActe"]['nomCanonique']+
            ","+
            json_[current_prefix][key]["texteAssocie"]+")"+clrTxtCns)
            //process.exit()
          }else if(Object.keys(json_[current_prefix][key]).includes("actesLegislatifs")){
            //console.log()
            actesLegislatifs_(json_[current_prefix][key], filename, previous_prefix+current_prefix+".")
          }
          //actesLegislatifs_(json_[current_prefix][key], filename, previous_prefix+current_prefix+"."+key+".")
        })
      }
      
      //actesLegislatifs2_(json_[current_prefix], filename, previous_prefix+current_prefix+".")
    }else{
      console.log("acteLegislatif_")
      console.log(previous_prefix+current_prefix+" " + filename)
      console.log(Object.keys(json_))
      console.log(json_)
      process.exit()
    }
  }
  
  //document.dossierParlementaire
  function actesLegislatifs_(json_: any, filename: string, previous_prefix:string){
    if(json_ == null || Object.keys(json_)==undefined){return;}
    const current_prefix="actesLegislatifs"
    process.stdout.write(current_prefix+"->")
    //document.dossierParlementaire.actesLegislatifs
    if(Object.keys(json_).includes(current_prefix)){
      acteLegislatif_(json_[current_prefix], filename, previous_prefix+current_prefix+".")
    }else{
      console.log("actesLegislatifs_")
      console.log(previous_prefix+current_prefix+" " + filename)
      console.log(Object.keys(json_))
      console.log(json_)
      process.exit()
    }
  }
  
  //document.dossierParlementaire
  function titreDossier_(json_: any, filename: string, previous_prefix:string){
    if(json_ == null || Object.keys(json_)==undefined){return;}
    const current_prefix="titreDossier"
    process.stdout.write(current_prefix+"->")
    //document.dossierParlementaire.actesLegislatifs
    if(Object.keys(json_).includes(current_prefix)){
      process.stdout.write(redTxtCns+"("+
            json_[current_prefix]["titre"]+
            ","+
            json_[current_prefix]["senatChemin"]+")"+clrTxtCns)
      //acteLegislatif_(json_[current_prefix], filename, previous_prefix+current_prefix+".")
  
    }else{
      console.log("titreDossier_")
      console.log(previous_prefix+current_prefix+" " + filename)
      console.log(Object.keys(json_))
      console.log(json_)
      process.exit()
    }
  }
  
  //document.
  function dossierParlementaire_(json_: any, filename: string){
    if(json_ == null || Object.keys(json_)==undefined){return;}
    //document.dossierParlementaire
    const current_prefix="dossierParlementaire"
    process.stdout.write(current_prefix+"->")
    if(Object.keys(json_).includes(current_prefix)){
      titreDossier_(json_[current_prefix], filename, current_prefix+".")
      actesLegislatifs_(json_[current_prefix], filename, current_prefix+".")
    }else{
      console.log("dossierParlementaire_")
      console.log(current_prefix+" "+ filename)
      console.log(Object.keys(json_))
      console.log(json_)
      process.exit()
    }
  }
  //*/
  
  async function retrieve_proposition(){
    const path='/usr/src/app/data/json/dossierParlementaire'
    fs.readdir(path,(err, files)=>{
      files.forEach((file)=>{
        fs.readFile(path+"/"+file, (err, data) => {
          if (err) throw err;
          const dossier = JSON.parse(data.toString());
          //console.log(data.toString())
          /*
          {
            "dossierParlementaire":{
              "actesLegislatifs":{
                "acteLegislatif":{
                  "callback",""
                }
              }
            }
          }
          */
          process.stdout.write("Try " + file + " -> ")
          dossierParlementaire_(dossier, file)
          console.log()
          //const keywords=["texteAssocie","senatChemin"]
          //console.log(find_keywords_in_json(keywords, dossier, []))
          /*
          const extraction=["dossierParlementaire","actesLegislatifs","acteLegislatif","actesLegislatifs"]
  
          
          console.log("\n")
          console.log(dossier.dossierParlementaire.titreDossier.senatChemin); 
          //dossierParlementaire_(dossier, file);
          console.log("aaa " + recursive_extraction(dossier, file, extraction, "dossier."))
          //recursive(extraction)
          /*
            if(Object.keys(dossier.dossierParlementaire).includes('actesLegislatifs') && dossier.dossierParlementaire.actesLegislatifs != null){
              if(Object.keys(dossier.dossierParlementaire.actesLegislatifs).includes('acteLegislatif') &&
                            dossier.dossierParlementaire.actesLegislatifs.acteLegislatif != null){
                console.log("dossierParlementaire.actesLegislatifs.acteLegislatif")
                console.log(Object.keys(dossier.dossierParlementaire.actesLegislatifs.acteLegislatif));
              
                if(Object.keys(dossier.dossierParlementaire.actesLegislatifs.acteLegislatif).includes('actesLegislatifs') &&
                       dossier.dossierParlementaire.actesLegislatifs.acteLegislatif.actesLegislatifs != null){
                  console.log("dossierParlementaire.actesLegislatifs.acteLegislatif.actesLegislatifs")
                  console.log(Object.keys(dossier.dossierParlementaire.actesLegislatifs.acteLegislatifs.actesLegislatifs));
                }
              
              }else{
                console.log("dossierParlementaire actesLegislatifs " + file)
                console.log(Object.keys(dossier.dossierParlementaire.actesLegislatifs));
              }
            }else{
              console.log("dossierParlementaire " + file)
              console.log(Object.keys(dossier.dossierParlementaire));
            }
          }//*/
          
          
          /*
          if( Object.keys(dossier.dossierParlementaire.actesLegislatifs).includes('actesLegislatifs') ){
            if(dossier.dossierParlementaire.actesLegislatifs.actesLegislatifs != null)
              console.log(dossier.dossierParlementaire.actesLegislatifs.actesLegislatifs);
          }else{
            console.log(dossier.dossierParlementaire.actesLegislatifs);
          }*/
        });
  
      })
    })
    console.log("Fin")
  }


  export default retrieve_proposition;