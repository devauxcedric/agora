import { DataTypes, Sequelize} from 'sequelize';

const sequelize = new Sequelize('app_db', 'app_user', 'secretpassword', {
    host: 'postgres',
    port: 5432,
    dialect: 'postgres'
  });

export  const PoliticalParty = sequelize.define('PoliticalParties',{
    id:{
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name:{
        type: DataTypes.STRING,
        allowNull: false
    }
},{
  freezeTableName: true,
});