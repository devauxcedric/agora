import {Sequelize, DataTypes} from 'sequelize';
import { Comments } from './CommentEntity';
import { PoliticalParty } from './PoliticalParty';
import { Politician } from './Politician';
import { Proposition, PropositionCreators, PropositionParty } from './Proposition';
import { Rating } from './Rating';
import { Users } from './UserEntity';

export {Comments, PoliticalParty, Politician, Proposition, PropositionCreators, PropositionParty, Rating, Users}

