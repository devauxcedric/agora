import { DataTypes, Sequelize} from 'sequelize';

const sequelize = new Sequelize('app_db', 'app_user', 'secretpassword', {
    host: 'postgres',
    port: 5432,
    dialect: 'postgres'
  });

export  const Rating = sequelize.define('Ratings',{
    id:{
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    rating:{
        type: DataTypes.INTEGER,
        allowNull: false
    },
    propositionId:{
        type: DataTypes.STRING,
    }
},{
  freezeTableName: true,
});