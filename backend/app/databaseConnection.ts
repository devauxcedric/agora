import { Sequelize } from 'sequelize';

export const sequelize = new Sequelize('app_db', 'app_user', 'secretpassword', {
    host: 'postgres',
    port: 5432,
    dialect: 'postgres'
  });
  
export default sequelize;