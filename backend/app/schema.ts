import { gql } from "apollo-server-express"; 

export const Schema = gql`
  type  Comments {
    id: Int!
    text: String
  }
  type Query {
    getAllComments: [Comments]
    getComment(id: Int): Comments
  }
  type  PoliticalParty {
    id: Int!
    name: String
  }
  type Query {
    getAllParties: [PoliticalParty]
    getParties(id: Int): PoliticalParty
  }
  type  Politician {
    id: Int!
    name: String
    contactAddress: String
  }
  type Query {
    getAllPoliticians: [Politician]
    getPolitician(id: Int): Politician
  }
  type  Proposition {
    id: Int!
    name: String
    status: String
  }
  type Query {
    getAllPropositions: [Proposition]
    getProposition(id: Int): Proposition
  }
  type  Rating {
    id: Int!
    rating: Int
  }
  type Query {
    getAllRatings: [Rating]
    getRating(id: Int): Rating
  }
`;